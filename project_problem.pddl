(define (problem prob)
	(:domain barbequePlanning)
	(:objects
		left right - hand
		grill1 - grill
		charcoal1 - charcoal
		beer - drink
		beef - meat
		knife1 - knife	
	)
	(:init 
		(onTable beef)
		(onTable beer)
		(onTable grill1)
		(onTable charcoal1)
		(onTable knife1)
		(onTable mici)
		(not (onGrill beef))
		(coldCharcoal charcoal1)
		(uninstalledGrill grill1)
		(unCooked beef)
		(notcutMeat beef)
		(fullBottle beer)
		(handEmpty left)
		(handEmpty right)
		(emptyGrill grill1)
		(not (cleanGrill grill1))
	)
	
	(:goal
		(and (not (notcutMeat beef))
		     (onTable beef)
		     (onTable knife1)
		)
	)
)

