(define (domain barbequePlanning)
  (:requirements :strips :typing)
  (:types 
       hand items - object
       instruments food tool - items
       grill charcoal - instruments
       meat drink - food  
       knife - tool
  )

  (:predicates
      (onGrill ?m - meat)
      (onTable ?i - items)
      (onGround ?i - instruments)
      (holdingF ?h - hand ?m - food)
      (handEmpty ?h - hand)
      (coldCharcoal ?c - charcoal)
      (uninstalledGrill ?g - grill)
      (unCooked ?m - meat)
      (holdingI ?i - instruments ?h1 - hand ?h2 - hand)
      (cooking)
      (drinking ?d - drink)
      (fullBottle ?d - drink)
      (emptyGrill ?g - grill)
      (cleanGrill ?g - grill)
      (notcutMeat ?m - meat)
      (holdingT ?h - hand ?t - tool)
  )
  
  (:action graspFood
  	:parameters (?h - hand ?i - food ?g - grill)
  	:precondition (and (onTable ?i)
  			    (handEmpty ?h)
  		      )
  	:effect (and (not (onTable ?i))
  		     (not (handEmpty ?h))
  		     (holdingF ?h ?i)
  		)
  
  )
  
  
  (:action leaveFoodOnTable
  	:parameters (?h - hand ?f - food)
  	:precondition(holdingF ?h ?f)
  	:effect(and (not (holdingF ?h ?f))
  	            (handEmpty ?h)
  	            (onTable ?f)
  		)
  )
  
  (:action leaveFoodOnGrill
  	:parameters (?h - hand ?m - meat ?g - grill)
  	:precondition (and (holdingF ?h ?m)
  		     	   (not (uninstalledGrill ?g))
  		     	   (unCooked ?m)
  		     	   (emptyGrill ?g)
  		     	   (cleanGrill ?g)
  		      )
  	:effect(and (not (holdingF ?h ?m))
  		     (handEmpty ?h)
  		     (onGrill ?m)
  		     (not (emptyGrill ?g))
  		)
  )
  
  (:action graspInstrument
  	:parameters (?h1 - hand ?h2 - hand ?i - instruments)
  	:precondition (and (onTable ?i)
  		            (handEmpty ?h1)
  		            (handEmpty ?h2)
  		      )
  	:effect (and (not (handEmpty ?h1))
  	 	     (not (handEmpty ?h2))
  	 	     (not (onTable ?i))
  	 	     (holdingI ?i ?h1 ?h2)
  		)
  )
  
  (:action leaveInstrument
  	:parameters (?h1 - hand ?h2 - hand ?i - instruments)
  	:precondition (holdingI ?i ?h1 ?h2)
  	:effect (and (handEmpty ?h1)
  		     (handEmpty ?h2)
  		     (not (holdingI ?i ?h1 ?h2))
  		     (onGround ?i)
  		)
  )
  
  (:action cleanTheGrill
  	:parameters (?g - grill ?h1 - hand ?h2 - hand)
  	:precondition (and (not (cleanGrill ?g))
  		            (handEmpty ?h1)
  		            (handEmpty ?h2)
  		       )
  	:effect (cleanGrill ?g)
  )
  
  (:action startFire 
  	:parameters (?c - charcoal)
  	:precondition (and  (coldCharcoal ?c)
  			    (onGround ?c)
  		      )
  	:effect (not (coldCharcoal ?c))
  )	
  	
  (:action installGrill
  	:parameters (?c - charcoal ?g - grill)
  	:precondition (and  (not (coldCharcoal ?c))
  			    (uninstalledGrill ?g)
  			    (onGround ?g)
  		      )
  	:effect (not (uninstalledGrill ?g))
  )
  
  (:action startCooking
  	:parameters (?g - grill ?m - meat ?d - drink )
  	:precondition (and (onGrill ?m)
  		           (unCooked ?m)
  		      )
  	:effect (and (cooking)
  		     (not (onTable ?d))  
  		)
  )
  
  (:action startDrink 
  	:parameters (?h - hand ?d - drink)
  	:precondition (and (holdingF ?h ?d)
  		           (cooking)
  		           (fullBottle ?d) 
  		      )
  	:effect (drinking ?d)
  )
  
  (:action drink
  	:parameters (?d - drink)
  	:precondition (drinking ?d)
  	:effect (not (fullBottle ?d))
  )
  
  (:action stopDrink
  	:parameters (?h - hand ?d - drink)
  	:precondition (and (not (fullBottle ?d))
  		           (onTable ?d)
  		      )
  	:effect (not (drinking ?d))
  )
  
  
  (:action stopCooking
  	:parameters (?g - grill ?m - meat ?d - drink)
  	:precondition (and (not (fullBottle ?d))
  	                   (not (drinking ?d))
  	                   (onTable ?d)
  		      )
  	:effect (not (unCooked ?m))
  )
  
  (:action takeFromGrill
  	:parameters (?m - meat ?g - grill) 
  	:precondition (and (not (unCooked ?m))
  		            (onGrill ?m)
  		       )
  	:effect (and (onTable ?m)
  		     (not (cleanGrill ?g))
  		)
  )
  
  (:action grabTool
  	:parameters (?h - hand ?t - tool)
  	:precondition (and (handEmpty ?h)
  			    (onTable ?t)
  		      )
  	:effect (and (not (handEmpty ?h))
  		     (not (onTable ?t))
  		     (holdingT ?h ?t)
  		)
  )
  
  (:action leaveTool
  	:parameters (?h - hand ?t - tool)
  	:precondition (holdingT ?h ?t)
  	:effect (and (handEmpty ?h)
  		     (not (holdingT ?h ?t))
  		     (onTable ?t)
  		)
  )
  
  (:action cutTheMeat
  	:parameters (?m - meat ?k - knife ?h1 - hand ?h2 - hand)
  	:precondition (and (onTable ?m)
  		           (not (unCooked ?m))
  		           (holdingT ?h1 ?k)
  		           (holdingF ?h2 ?m)
  		           (notcutMeat ?m)
  		      )
  	:effect (and (not (notcutMeat ?m))
  		     (onTable ?m)
  		     (onTable ?k)
  		)
  )
)

